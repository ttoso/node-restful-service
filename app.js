//To serve the API
const express = require('express');
const app = express();
//Integration with mongoDB databases
const mongoose = require('mongoose');
//Middleware to parse request bodies
const bodyParser = require('body-parser');
//File to hide credentials(The database used don't have it)
require('dotenv/config');

app.use(bodyParser.json());

//Import routes declared in their folder
const postRoute = require('./routes/posts');
//Set route /posts to all resources delclared in posts file
app.use('/posts', postRoute);

//Connection to the database
mongoose.connect(process.env.BD_CONNECTION,
    { useNewUrlParser: true},
    () => {
    console.log('Connected to DB');    
});

//Serve the api and specify port
app.listen(3000);
