# Node RESTFul Service
Example of a simple restful service based in node with express and MongoDB.
Connection implemented using mongoose package for presistent access.
Example of operations to GET, POST, PUT and DELETE.

## Used node packages
- npm init to generate package.json
- npm install express
- npm install nodemon
- npm install mongoose
- npm install dotenv
- npm install body-parser

## Folders structure
- models: All schemas for DB entities
- routes: Router fles with resources declared
- app.js: Entrypoint for the API
- .env: Constants with information that shouldn't be hardcoded


## Use the app
- npm install
- npm run dev (Service at port 3000)