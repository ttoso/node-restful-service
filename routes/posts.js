const express = require('express');
const router = express.Router();
const Post = require('../models/Post');


//GET posts
router.get('/', async (req, res) => {
    try {
        const posts = await Post.find();
        res.json(posts);
    } catch (error) {
        res.json({message: error});
    }
});


//POST a new post
router.post('/', async (req, res) => {
    console.log(req.body.description);
    const post = new Post({
        title: req.body.title,
        description: req.body.description
    })
    try {
        const savedPost = await post.save();
        res.json(savedPost);
    } catch (error) {
        res.json({ message: error });
    }
});

//GET specific post by id
router.get('/:postId', async (req, res) => {
    try {
        const post = await Post.findById(req.params.postId);
        res.json(post);
    } catch (error) {
        res.json({message: error});
    }
});


//DELETE a post by id
router.delete('/:postId', async (req, res) => {
    try {
        const removedPost = await Post.remove({_id: req.params.postId});
        res.json(removedPost);
    } catch (error) {
        res.json({message: error});
    }
});


//UPDATE a post by id
router.put('/:postId', async (req, res) => {
    try {
        const modPost = await Post.updateOne(
            { _id: req.params.postId },
            { $set : { title: req.body.title } }
        );
        res.json(modPost);
    } catch (error) {
        res.json({message: error});
    }
});

module.exports = router;